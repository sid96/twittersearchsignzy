package com.tcm.twittersearch.model;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

/**
 * Created by sid on 24-02-2018.
 */
public class TweetGrabber {
    private final List<Status> statuses;

    public TweetGrabber(List<Status> statuses) {
        this.statuses = statuses;
    }

    public static class Status {
        @SerializedName("created_at")
        private final Date createdAt;

        @SerializedName("retweet_count")
        private final String retweetCount;

        private final String full_text;

        private final User user;

        private static class User {
            private final String name;

            @SerializedName("screen_name")
            private final String screenName;

            @SerializedName("profile_image_url")
            private final String user_image;

            private User(String name, String screenName, String user_image) {
                this.name = name;
                this.screenName = screenName;
                this.user_image = user_image;
            }

            public String getName() {
                return name;
            }

            public String getUser_image() { return user_image; }

            public String getScreenName() {
                return screenName;
            }
        }


        public Status(Date createdAt, String retweetCount, String text, User user) {
            this.createdAt = createdAt;
            this.full_text = text;
            this.user = user;
            this.retweetCount = retweetCount;
        }

        public Date getCreatedAt() {
            return createdAt;
        }

        public String getRetweetCount() { return retweetCount; }

        public String getText() {
            return full_text;
        }

        public String getUserName() {
            return user.getName();
        }

        public String getUserImage() {return user.getUser_image(); }

        public String getUserScreenName() {
            return user.getScreenName();
        }

    }

    public List<Status> getStatuses() {
        return statuses;
    }
}


package com.tcm.twittersearch.model;

import java.io.Serializable;

/**
 * Created by sid on 25-02-2018.
 */
public class TweetAnalyzer implements Serializable{

    String content;
    int retweet_count;
    int atCounts;

    public int getAtCounts() {
        return atCounts;
    }

    public void setAtCounts(int atCounts) {
        this.atCounts = atCounts;
    }

    public int getRetweet_count() {
        return retweet_count;
    }

    public void setRetweet_count(int retweet_count) {
        this.retweet_count = retweet_count;
    }

    public String getContent() {

        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

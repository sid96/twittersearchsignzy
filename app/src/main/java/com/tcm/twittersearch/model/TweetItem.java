package com.tcm.twittersearch.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by sid on 24-02-2018.
 */

public class TweetItem implements Parcelable {
    private final String username;
    private final String content;
    private final Date createdAt;
    private final String retweetCount;
    private final String user_image;

    public TweetItem(String username, String content, Date createdAt, String retweetCount, String user_image) {
        this.username = username;
        this.content = content;
        this.createdAt = createdAt;
        this.user_image = user_image;
        this.retweetCount = retweetCount;
    }

    protected TweetItem(Parcel in) {
        username = in.readString();
        content = in.readString();
        createdAt = new Date(in.readLong());
        retweetCount = in.readString();
        user_image = in.readString();
    }

    public static final Creator<TweetItem> CREATOR = new Creator<TweetItem>() {
        @Override
        public TweetItem createFromParcel(Parcel in) {
            return new TweetItem(in);
        }

        @Override
        public TweetItem[] newArray(int size) {
            return new TweetItem[size];
        }
    };

    public static List<TweetItem> buildTweets(TweetGrabber statuses) {
        List<TweetItem> tweetList = new ArrayList<>();
        for (TweetGrabber.Status status : statuses.getStatuses()) {
            String content = status.getText();
            String username = status.getUserName();
            Date createdAt = status.getCreatedAt();
            String retweetCount = status.getRetweetCount();
            String imageUrl = status.getUserImage();
            tweetList.add(new TweetItem(username, content, createdAt, retweetCount, imageUrl));
        }
        return tweetList;
    }

    public String getUsername() {
        return username;
    }

    public String getUserImage() {return user_image;}

    public String getContent() {
        return content;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public String getRetweetCount() {return retweetCount; }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(username);
        parcel.writeString(content);
        parcel.writeLong(createdAt.getTime());
        parcel.writeString(retweetCount);
        parcel.writeString(user_image);
    }
}

package com.tcm.twittersearch.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tcm.twittersearch.R;
import com.tcm.twittersearch.model.TweetItem;

import java.util.List;


/**
 * Created by sid on 25-02-2018.
 */
public class TweetRecyclerAdapter extends RecyclerView.Adapter<TweetRecyclerAdapter.TweetViewHolder> {

    private List<TweetItem> tweets;
    private Context con;
    public TweetRecyclerAdapter(List<TweetItem> tweets){
        this.tweets = tweets;
    }
    @Override
    public TweetViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        con = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tweet_row_item,parent,false);
        return new TweetViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TweetViewHolder holder, int position) {

        holder.tweet_text.setText(tweets.get(position).getContent());
        holder.published.setText(tweets.get(position).getCreatedAt()+"");
        holder.username.setText("@"+tweets.get(position).getUsername());
        holder.retweet.setText("Retweets: "+tweets.get(position).getRetweetCount());
        Glide.with(con).load(tweets.get(position).getUserImage()).placeholder(R.drawable.avatar).error(R.drawable.avatar).dontAnimate().into(holder.user_image);
        //+"\nRetweet Count: "+tweets.get(position).getRetweetCount());
    }

    @Override
    public int getItemCount() {
        return tweets.size();
    }

    public class TweetViewHolder extends RecyclerView.ViewHolder
    {
        TextView tweet_text, published, retweet, username;
        ImageView user_image;

        public TweetViewHolder(View itemView) {
            super(itemView);
            tweet_text = (TextView)itemView.findViewById(R.id.tweet_full_text);
            published = (TextView)itemView.findViewById(R.id.user_post);
            retweet =  (TextView)itemView.findViewById(R.id.user_retweets);
            username =  (TextView)itemView.findViewById(R.id.user_name);
            user_image = (ImageView)itemView.findViewById(R.id.user_image);
        }
    }
}

package com.tcm.twittersearch;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.tcm.twittersearch.adapter.TweetRecyclerAdapter;
import com.tcm.twittersearch.model.TweetAnalyzer;
import com.tcm.twittersearch.model.TweetGrabber;
import com.tcm.twittersearch.model.TweetItem;
import com.tcm.twittersearch.network.rest.ApiClient;
import com.tcm.twittersearch.network.rest.ApiInterface;
import com.tcm.twittersearch.network.rest.Oauth2Token;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Dashboard extends AppCompatActivity {

    RecyclerView recyclerView;
    RelativeLayout rl;
    LinearLayout ll;
    RecyclerView.LayoutManager layoutManager;
    TweetRecyclerAdapter tweetRecyclerAdapter;
    List<TweetItem> tweets;
    ArrayList<TweetAnalyzer> tweet_content;
    ApiInterface apiInterface;
    private String mConsumerKey;
    private String mConsumerSecret;
    Call<TweetGrabber> mTweetcall;
    Call<Oauth2Token> mTokenCall;
    String access;
    Button analyze;

    @Override
    public void onBackPressed() {
        //
        final AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(Dashboard.this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(Dashboard.this);
        }
        builder.setTitle("Exit App")
                .setMessage("Are you sure you want to exit the app")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                        dialog.dismiss();
                    }
                })
                .show();
    }

    //    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater in = getMenuInflater();
//        in.inflate(R.menu.menu_main,menu);
//        return super.onCreateOptionsMenu(menu);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if(item.getItemId()==R.id.top3)
//        {
//            try {
//                if (tweet_content == null || tweet_content.size() <= 0)
//                    Toast.makeText(MainActivity.this, "No tweets to analyse!", Toast.LENGTH_SHORT).show();
//                else
//                    startActivity(new Intent(MainActivity.this, TweetAnalyzer.class).putExtra("tweets", tweet_content));
//            }catch (Exception e)
//            {
//                Log.d("Exception : menu_main",e.toString());
//            }
//
//        }
//        return super.onOptionsItemSelected(item);
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        recyclerView = (RecyclerView)findViewById(R.id.tweet_holder);
        analyze = (Button)findViewById(R.id.top3);
        ll =(LinearLayout)findViewById(R.id.llayout);
        rl = (RelativeLayout)findViewById(R.id.pdialog);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        rl.setVisibility(View.VISIBLE);

        mConsumerKey = "k3EMSqzO4CpC5jZGVZX2hafpf";
        mConsumerSecret ="4Ee6iFH1GTAFl8XF4qbK1bbJLgsh1sbeYV2aB17YOyIsteXzH9";

        apiInterface = ApiClient.getApiClient().create(ApiInterface.class);

        mTokenCall = apiInterface.getAccessToken(getAuthorizationHeader(), "client_credentials");
        mTokenCall.enqueue(new Callback<Oauth2Token>() {
            @Override
            public void onResponse(Call<Oauth2Token> call, Response<Oauth2Token> response) {
                if (response.isSuccessful()) {
                    saveAccessToken(response.body().getAccessToken());
                }
            }

            @Override
            public void onFailure(Call<Oauth2Token> call, Throwable t) {
                Toast.makeText(Dashboard.this, "Something went wrong !", Toast.LENGTH_SHORT).show();
            }
        });

        analyze.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (tweet_content == null || tweet_content.size() <= 0)
                        Toast.makeText(Dashboard.this, "No tweets to analyse!", Toast.LENGTH_SHORT).show();
                    else
                        startActivity(new Intent(Dashboard.this, TweetAnalysis.class).putExtra("tweets", tweet_content));
                }catch (Exception e)
                {
                    Log.d("Exception : menu_main",e.toString());
                }
            }
        });

    }

    private void runLayoutAnimation(final RecyclerView recyclerView) {
        final Context context = recyclerView.getContext();
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation_fall_down);

        recyclerView.setLayoutAnimation(controller);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }

    private int count(String text, char c)
    {
        int counter = 0;
        for( int i=0; i<text.length(); i++ ) {
            if( text.charAt(i) == c ) {
                counter++;
            }
        }
        return counter;
    }

    private void copyContent(List<TweetItem> data){
        tweet_content = new ArrayList<>();
        for(int i=0;i<data.size();i++)
        {
            TweetAnalyzer tr = new TweetAnalyzer();
            tr.setContent(data.get(i).getContent());
            tr.setRetweet_count(Integer.parseInt(data.get(i).getRetweetCount()));
            tr.setAtCounts(count(data.get(i).getContent(),'@'));
            tweet_content.add(tr);
        }
    }

    private void saveAccessToken(String accessToken) {
        access = "Bearer " + accessToken;
        Log.d ("access",access);
        mTweetcall = apiInterface.getTweets(access,"ClearTax","100","recent","extended");
        mTweetcall.enqueue(new Callback<TweetGrabber>() {
            @Override
            public void onResponse(Call<TweetGrabber> call, Response<TweetGrabber> response) {
                if (response.isSuccessful()) {
                    Log.d("response",response.body().toString());
                    rl.setVisibility(View.GONE);
                    ll.setVisibility(View.VISIBLE);
                    tweets = TweetItem.buildTweets(response.body());
                    copyContent(tweets);
                    tweetRecyclerAdapter = new TweetRecyclerAdapter(tweets);
                    recyclerView.setAdapter(tweetRecyclerAdapter);
                    runLayoutAnimation(recyclerView);
                }
            }

            @Override
            public void onFailure(Call<TweetGrabber> call, Throwable t) {
                Log.d("Error","Something Happened !"+call.toString()+" and "+t.toString());
            }
        });
    }

    private String getAuthorizationHeader() {
        try {
            String consumerKeyAndSecret = mConsumerKey + ":" + mConsumerSecret;
            byte[] data = consumerKeyAndSecret.getBytes("UTF-8");
            String base64 = Base64.encodeToString(data, Base64.NO_WRAP);

            return "Basic " + base64;
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    private void safeStop(Call call) {
        if (call != null && !call.isCanceled()) {
            call.cancel();
        }
    }
}
